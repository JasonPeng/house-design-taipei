## 家庭成員
 - 兩個大人
 - 一個小孩
 - 兩隻貓

## 整體空間規劃方向
 * 希望顧慮到小朋友
 * 貓咪友善的環境
    - 貓砂盆, 可隱藏最好
    - 貓咪吃飯的地方，會有飼料以及水盆
 * 線路能夠隱藏
 * 空間感(利用玻璃門製造穿視的效果?)
 * 木質地板

## 房間規劃
 * 一個主臥
 * 一個小朋友的房間
 * 一個書房或是大的工作桌面

### 主臥
 * 雙人床 (床可考慮不要床架)
    ![bed](bed.png "bed")
    ![bed-2](bed-2.png "bed-2")
 * 需要強大的衣物收納空間
 * 化妝空間

### 主臥浴室
 * 希望好清潔，不易生霉或看不出來
 * 喜歡這樣的色調及獨立浴缸&洗手台
    ![Main bathroom](main-bathroom.png "main-bathroom")

### 次臥
 * 單人床

### 辦公區域
 * 主要男主人工作用，需要一個較大的桌面放置電腦

## 客浴
 * 希望隱藏式入口

## 其它
 * 開放廚房
    - 我有發信詢問建商，確定廚房牆面不能打掉，是否代表不可行呢？
 * 需要有空間儲放行李箱(兩大)

## 參考風格
 * 喜歡這種溫暖的無印風格 [style1](https://www.mobile01.com/newsdetail/27845/comfortable-house)
 * 間北歐風的整體感我們也很喜歡 [style2](https://www.mobile01.com/newsdetail/28290/nordic-style-house)
 * 希歡這樣的跳色牆面 [style3](https://www.mobile01.com/newsdetail/27257/less-is-more)
 * 嚮往這種感覺，也知道空間跟條件差距太多，哈哈  [style4](https://www.mobile01.com/newsdetail/24217/mobile01-tainan-selfbuilt-shen)
 * 喜歡風格參考照片
     ![style-1](style-1.jpg "style-1")
